﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Threading;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Media.Animation;

namespace MyFashions1Nov
{
    /// <summary>
    /// Interaction logic for CategoryUC.xaml
    /// </summary>
    public partial class CategoryUC : UserControl
    {
        public CreateDBEntity.Models.MainCategory Categ;
        public CreateDBEntity.Models.SubCategory MainSubCateg;
        public CreateDBEntity.Models.Category SubCateg;
        public CreateDBEntity.Models.productTemplateBeans ptBean;
        public CreateDBEntity.Models.Store Store;
        public myCustomItemsControl listing;
        public CategoryUC()
        {
            InitializeComponent();
            txtCount.Text = "-0.33";
        }

        public event MyeventHandler MyEventHand;
        public event MyeventHandler MyEventStackPanel;
        public delegate void MyeventHandler(object sender, EventArgs e);
        
        public string CountIndex(int val)
        {
            decimal retValue = (decimal)-0.28;
            switch (val)
            {
                case 1:
                    retValue = (decimal)-0.25;
                    break;
                case 2:
                    retValue = (decimal)-0.29;
                    break;
                case 3:
                    retValue = (decimal)-0.33;
                    break;
                case 4:
                    retValue = (decimal)-0.37;
                    break;
                case 5:
                    retValue = (decimal)-0.42;
                    break;
                case 6:
                    retValue = (decimal)-0.46;
                    break;
                case 7:
                    retValue = (decimal)-0.47;
                    break;
                case 8:
                    retValue = (decimal)-0.55;
                    break;
                case 9:
                    retValue = (decimal)-0.565;
                    break;
                case 10:
                    retValue = (decimal)-0.58;
                    break;
                case 11:
                    retValue = (decimal)-0.67;
                    break;
                case 12:
                    retValue = (decimal)-0.68;
                    break;
                default:
                    retValue = (decimal)-0.58;
                    break;
            }
            return retValue.ToString();
        }


        public void Refresh()
        {
            try
            {
                LogHelper.Logger.Info("Category Refresh Starts");
                Categ = this.DataContext as CreateDBEntity.Models.MainCategory;
                txtCount.Text = CountIndex(this.categoryContext.Items.Count);
                MyEventStackPanel(this, null);
                LogHelper.Logger.Info("Category Refresh Ends");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: CategoryUC : Refresh: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }            
        }

        private void BtnItem_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                Categ = btn.DataContext as CreateDBEntity.Models.MainCategory;
                SubCateg = btn.DataContext as CreateDBEntity.Models.Category;
                Storyboard MyStoryboard;
                LogHelper.Logger.Info("When ButtonItem TouchDown Starts");
                if (Categ != null)
                {
                    if (Categ.lstCategory.Count == 1)
                    {
                        stkPnlMainHeading.DataContext = Categ.lstCategory[0];
                        categoryContext.ItemsSource = Categ.lstCategory[0].subCategory.Where(c=>c.check==true).OrderBy(c => c.id).ToList();
                        txtCount.Text = CountIndex(categoryContext.Items.Count);// Categ.lstCategory[0].subCategory.Count);
                    }
                    else
                    {
                        stkPnlMainHeading.DataContext = Categ.lstCategory;
                        categoryContext.ItemsSource = Categ.lstCategory.Where(c => c.check == true).OrderBy(c => c.id).ToList();

                        txtCount.Text = CountIndex(categoryContext.Items.Count); //CountIndex(Categ.lstCategory.Count);
                    }
                    MyEventStackPanel(this, null);
                    stkPnlMainHeading.DataContext = Categ;
                }
                else if (SubCateg != null)
                {
                    stkPnlMainHeading.DataContext = SubCateg.subCategory;
                    categoryContext.ItemsSource = SubCateg.subCategory.Where(c=>c.check==true);
                    txtCount.Text = CountIndex(categoryContext.Items.Count);//SubCateg.subCategory.Count);
                    stkPnlMainHeading.DataContext = SubCateg;
                    
                    MyStoryboard = FindResource("CtgListingSB") as Storyboard;
                    MyStoryboard.Stop();
                    
                    RenderTransform = new MatrixTransform(0.8, 0, 0, 0.8, 10, 10);
                }
                
                else
                {
                    MyStoryboard = (Storyboard)this.FindResource("CtgListingSB") as Storyboard;
                    MyStoryboard.Begin();

                    MyEventHand(btn.DataContext, null);
                }

                

                LogHelper.Logger.Info("When ButtonItem TouchDown Stops");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: CategoryUCPage : BtnItem_TouchDown: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }
            
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
