﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyFashions1Nov
{
    /// <summary>
    /// Interaction logic for Compare.xaml
    /// </summary>
    public partial class Compare : UserControl
    {
        public Compare()
        {
            InitializeComponent();
        }
        //ListView listView;
        //ListViewItem listViewItem;
        public Close Cl;
        CreateDBEntity.Models.productTemplateBeans samp;
        public event EventHandler MyEventCartCntUpdate;
        public event EventHandler MyEventCmpreCntUpdate;
        int i = 20;
        void Cl_myCartEvent(object sender, EventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Started Dragging From Compare To Cart");
                Cl = sender as Close;
                CreateDBEntity.Models.productTemplateBeans samp = Cl.DataContext as CreateDBEntity.Models.productTemplateBeans;
                if (!StoreCompare_Cart.lstCart.Contains(samp))
                {
                    StoreCompare_Cart.lstCart.Insert(0, samp);
                }
                StoreCompare_Cart.cartCnt = StoreCompare_Cart.lstCart.Count;
                StoreCompare_Cart.CmpreCnt = StoreCompare_Cart.lstCompare.Count;
                MyEventCmpreCntUpdate(this, null);
                MyEventCartCntUpdate(this, null);
                canvas1.Children.Remove(Cl);

                StoreCompare_Cart.lstSelectedCompare.Remove(samp);


                LogHelper.Logger.Info("Dragged From Compare To Cart");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ComparePage : Cl_CartEvent: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }

        }

        void Cl_myCloseEvent(object sender, EventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts dragging from Compare to Delete");
                Cl = sender as Close;
                canvas1.Children.Remove(Cl);
                StoreCompare_Cart.CmpreCnt = StoreCompare_Cart.lstCompare.Count;
                MyEventCmpreCntUpdate(this, null);
                LogHelper.Logger.Info("Dragged from Compare to Delete");

                StoreCompare_Cart.lstSelectedCompare.Remove(samp);

            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ComparePage : Cl_myCloseEvent: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }

        }

        private void Canvas_DragEnter(object sender, DragEventArgs e)
        {
            if (!(e.Data.GetDataPresent("contact")) || (sender == e.Source))
            {
                e.Effects = DragDropEffects.None;
            }
        }

        Point enterPoint;
        //private static T FindAnchestor<T>(DependencyObject current) where T : DependencyObject
        //{
        //    do
        //    {
        //        if (current is T)
        //        {
        //            return (T)current;
        //        }
        //        current = VisualTreeHelper.GetParent(current);
        //    }
        //    while (current != null);

        //    return null;
        //}

        private void Canvas_DragOver(object sender, DragEventArgs e)
        {
            enterPoint = e.GetPosition(this.canvas1);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            i = 20;
            LstParent.ItemsSource = StoreCompare_Cart.lstCompare.ToList();
            canvas1.ManipulationStarting += new EventHandler<ManipulationStartingEventArgs>(Canvas1_ManipulationStarting);
            canvas1.ManipulationDelta += new EventHandler<ManipulationDeltaEventArgs>(Canvas1_ManipulationDelta);

            lstthumbs = new List<string>();

        }

        void Canvas1_ManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            UIElement element = args.Source as UIElement;
            MatrixTransform xform = element.RenderTransform as MatrixTransform;
            Matrix matrix = xform.Matrix;

            ManipulationDelta delta = args.DeltaManipulation;
            Point center = args.ManipulationOrigin;
            matrix.Translate(-center.X, -center.Y);
            matrix.Scale(delta.Scale.X, delta.Scale.Y);
            matrix.Translate(center.X, center.Y);
            matrix.Translate(delta.Translation.X, delta.Translation.Y);
            xform.Matrix = matrix;

            args.Handled = true;
            base.OnManipulationDelta(args);
        }

        string str;
        void Canvas1_ManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = canvas1;
            args.Mode = currentMode;

            // Adjust Z-order
            FrameworkElement element = args.Source as FrameworkElement;
            Panel pnl = element.Parent as Panel;

            for (int i = 0; i < pnl.Children.Count; i++)
            {
                Panel.SetZIndex(pnl.Children[i],
                    pnl.Children[i] == element ? pnl.Children.Count : i);
            }

            // Set Pivot
            Point center = new Point(element.ActualWidth / 2, element.ActualHeight / 2);
            str += "X: " + center.X + " Y: " + center.Y + "\n";
            center = element.TranslatePoint(center, this);
            args.Pivot = new ManipulationPivot(center, 48);

            args.Handled = true;

            base.OnManipulationStarting(args);
        }

        ManipulationModes currentMode = ManipulationModes.All;
        private void LstParent_TouchDown_1(object sender, TouchEventArgs e)
        {
            ItemsControl sourceItemsControl = (ItemsControl)sender;
            var visual = e.OriginalSource as Visual;

            Window topWindow = Window.GetWindow(sourceItemsControl);
            Point initialMousePosition = e.GetTouchPoint(topWindow).Position;

            FrameworkElement sourceItemContainer = sourceItemsControl.ContainerFromElement(visual) as FrameworkElement;
            if (sourceItemContainer != null)
            {
                samp = sourceItemContainer.DataContext as CreateDBEntity.Models.productTemplateBeans;
            }
            else
                samp = null;
        }

        private void BtnClearAll_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                canvas1.Children.Clear();
                StoreCompare_Cart.CmpreCnt = LstParent.Items.Count;
                MyEventCmpreCntUpdate(this, null);
                foreach (var item in StoreCompare_Cart.lstSelectedCompare)
                {
                    StoreCompare_Cart.lstCompare.Remove(item);
                }
                StoreCompare_Cart.lstSelectedCompare.Clear();
                i = 20;
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: Compare Page : BtnClearAll_TouchDown: " + ex.Message, ex);
                myfashionsecurity.AuthenticationCodeLogic.sendLogEMail("C:/myfashions/Logs/myfashionApp_" + DateTime.Now.ToString("MMddyyyy"));
            }

        }

        List<string> lstthumbs;

        private void Canvas1_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            TouchPoint tpnt = e.GetTouchPoint(canvas1);
            if (tpnt.Position.X < 90 && tpnt.Position.X > -75 && tpnt.Position.Y > -70 && tpnt.Position.Y < 80)
            {
                this.Cursor = Cursors.Hand;
                Close cls = e.Source as Close;
                if (cls != null)
                {
                    cls.BtnCart_TouchDown_1(sender, e);
                }
            }
            else if (tpnt.Position.X < 90 && tpnt.Position.X > -75 && tpnt.Position.Y > (canvas1.ActualHeight - 80))
            {
                this.Cursor = Cursors.Hand;
                Close cls = e.Source as Close;
                if (cls != null)
                {
                    cls.BtnDelete_TouchDown_1(sender, e);
                }
            }
        }

        private void Canvas1_PreviewTouchMove_1(object sender, TouchEventArgs e)
        {
            TouchPoint tpnt = e.GetTouchPoint(canvas1);
            if (tpnt.Position.X < -5 && tpnt.Position.X > -75 && tpnt.Position.Y > 0 && tpnt.Position.Y < 80)
            {
                this.Cursor = Cursors.Hand;
            }
            else if (tpnt.Position.X < -5 && tpnt.Position.X > -75 && tpnt.Position.Y > (canvas1.ActualHeight - 80))
            {
                this.Cursor = Cursors.Hand;
            }
            else
            {
                this.Cursor = Cursors.Arrow;
            }

        }

        private void LstParent_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void LstParent_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            if (samp != null)
            {
                CreateDBEntity.Models.productTemplateBeans prodAlreadyExists = StoreCompare_Cart.lstSelectedCompare.Where(c => c.ProdTemplateID == samp.ProdTemplateID).FirstOrDefault();
                if (samp != null && prodAlreadyExists == null)
                {
                    this.Cursor = Cursors.Hand;
                    TouchPoint tPnt = e.GetTouchPoint(canvas1);
                    if (tPnt.Position.X >= 0 && tPnt.Position.X <= 1052 && tPnt.Position.Y > 0)
                    {
                        try
                        {
                            if (i > 760)
                            {
                                i = 20;
                            }

                                StoreCompare_Cart.lstSelectedCompare.Add(samp);
                                MatrixTransform trsfrm = new MatrixTransform(0.5, 0, 0, 0.5, i + 50, 50);
                                Cl = new Close();
                                Cl.MyCartEvent += Cl_myCartEvent;
                                Cl.MyCloseEvent += Cl_myCloseEvent;
                                Cl.Name = "Close" + i;
                                Cl.IsManipulationEnabled = true;
                                Cl.Samp = samp;
                                Cl.RenderTransform = trsfrm;
                                Cl.BringIntoView();
                                Panel.SetZIndex(Cl, 9999);

                                //StoreCompare_Cart.lstSelectedCompare.Add(samp);
                                canvas1.Children.Add(Cl);
                                i = i + 50;

                                StoreCompare_Cart.lstCompare.Remove(samp);
                                StoreCompare_Cart.CmpreCnt = StoreCompare_Cart.lstCompare.Count;
                                MyEventCartCntUpdate(this, null);
                                MyEventCmpreCntUpdate(this, null);

                                LstParent.ItemsSource = StoreCompare_Cart.lstCompare.ToList();
                                LstParent.Items.Refresh();
                        }
                        catch (Exception)
                        {

                        }
                    }

                    this.Cursor = Cursors.Arrow;
                }
                else
                {
                    this.Cursor = Cursors.Arrow;
                }
            }
        }

        private void LstParent_PreviewTouchMove_2(object sender, TouchEventArgs e)
        {
            TouchPoint tPnt = e.GetTouchPoint(canvas1);
            if (tPnt.Position.X >= 0 && tPnt.Position.X <= 1052 && tPnt.Position.Y > 0)
            {
                this.Cursor = Cursors.Hand;
            }
            else if (tPnt.Position.X >= 0 && tPnt.Position.Y > (canvas1.ActualHeight - 80))
            {
                this.Cursor = Cursors.Hand;
            }
            else
            {
                this.Cursor = Cursors.Arrow;
            }
        }
    }
}