﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MyFashions1Nov.Converters
{
    public class ConveretedPrice:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long prodTemplateID = (long)value;
            if (prodTemplateID > 0)
            {
                CreateDBEntity.Models.productTemplateBeans prodTemplateBean = StoreCompare_Cart.lstCart.Where(c => c.ProdTemplateID == prodTemplateID).SingleOrDefault();
                if (!prodTemplateBean.category.Name.Contains("JEWELLERY"))
                {
                    if (prodTemplateBean.discount > 0)
                        return "INR " + Math.Round(System.Convert.ToDecimal((prodTemplateBean.TemplatePrice - (prodTemplateBean.TemplatePrice * (prodTemplateBean.discount / 100))))).ToString();
                    else
                        return "INR " + Math.Round(prodTemplateBean.TemplatePrice);
                }
                else
                    return "Contact Sales Person for price.";
            }
            else
                return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
